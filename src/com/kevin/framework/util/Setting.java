package com.kevin.framework.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

public class Setting {

	/**
	 * 获取应用程序版本名称信息
	 * @return APP version
	 */
	public static String getAppVersionName(Context context) {
		if(context == null) {
			return null;
		}
		PackageManager manager = context.getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
			return info.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return "版本号未知";
	}
	
	/**
	 * 获取应用程序版本号
	 * @return APP version
	 */
	public static int getAppVersionCode(Context context) {
		if(context == null) {
			return 0;
		}
		PackageManager manager = context.getPackageManager();
		try {
			PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
			return info.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * 获取应用程序包名
	 */
	public static String getAppPackageName(Context context) {
		if (context == null) {
			return null;
		}
		return context.getPackageName();
	}
}
