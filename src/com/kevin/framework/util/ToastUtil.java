package com.kevin.framework.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.kevin.framework.Constant;
import com.kevin.framework.R;

/**
 * @description 自定义Toast
 * @author tianshaokai
 * @date 2014-11-12 9:44 AM
 * @email 113869kai@sina.com
 * @version 1.0
 */
@SuppressLint("InflateParams")
public class ToastUtil {
	
	private static Toast myToast;
	private static View mView;
	private static TextView textView;

	/**
	 * 弹出 短时间的Toast
	 * 
	 * @param context
	 *            上下文
	 * @param content
	 *            内容
	 */
	public static void showShort(Context context, String content) {
		if(context == null) {
			return;
		}
		Toast.makeText(context, content, Toast.LENGTH_SHORT).show();
	}
	
	public static void showShort(Context context, int r_id) {
		if(context == null) {
			return;
		}
		Toast.makeText(context, r_id, Toast.LENGTH_SHORT).show();
	}

	/**
	 *	弹出上时间的toast
	 * 
	 * @param context
	 *            上下文
	 * @param content
	 *            内容
	 */
	public static void showLong(Context context, String content) {
		if(context == null) {
			return;
		}
		Toast.makeText(context, content, Toast.LENGTH_LONG).show();
	}
	
	public static void showLong(Context context, int r_id) {
		if(context == null) {
			return;
		}
		Toast.makeText(context, r_id, Toast.LENGTH_LONG).show();
	}
	
	/**
	 * 显示自定义 Toast
	 * 
	 * @param context
	 *            上下文
	 * @param content
	 *            显示内容
	 * @param gravityStyle
	 *            位置
	 */
	public static void showMyToast(Context context, String content,
			int gravityStyle) {
		if(context == null) {
			return;
		}
		if (myToast == null) {
			myToast = new Toast(context);
			mView = LayoutInflater.from(context).inflate(R.layout.my_toast, null);
			textView = (TextView) mView.findViewById(R.id.my_toast_textview_status);
			setToast(content, gravityStyle);
		} else {
			setToast(content, gravityStyle);
		}
		myToast.show();
	}

	/**
	 * 设置 Toast 信息
	 * 
	 * @param content
	 *            消息内容
	 * @param gravityStyle
	 *            位置
	 */
	private static void setToast(String content, int gravityStyle) {
		textView.setText(content);
		// 设置到Toast中
		myToast.setView(mView);
		if (gravityStyle == Constant.TOAST_IN_TOP) {
			myToast.setGravity(Gravity.TOP, 0, 70);
		} else if (gravityStyle == Constant.TOAST_IN_CENTER) {
			myToast.setGravity(Gravity.CENTER, 0, 0);
		} else if (gravityStyle == Constant.TOAST_IN_BOTTOM) {
			myToast.setGravity(Gravity.BOTTOM, 0, 0);
		}
		myToast.setDuration(Toast.LENGTH_SHORT);
	}
	

}
