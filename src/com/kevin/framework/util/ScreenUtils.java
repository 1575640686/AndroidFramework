package com.kevin.framework.util;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class ScreenUtils {

	/**
	 * 获得屏幕宽度
	 */
	public static int getScreenWidth(Context context) {
		if(context == null) {
			return 0;
		}
		DisplayMetrics outMetrics = getDisplayMetrics(context);
		return outMetrics.widthPixels;
	}
	
	
	
	/**
	 * 获得屏幕高度
	 */
	public static int getScreenHeight(Context context) {
		if(context == null) {
			return 0;
		}
		DisplayMetrics outMetrics = getDisplayMetrics(context);
		return outMetrics.heightPixels;
	}

	private static DisplayMetrics getDisplayMetrics(Context context) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics outMetrics = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(outMetrics);
		return outMetrics;
	}
	
	
}
