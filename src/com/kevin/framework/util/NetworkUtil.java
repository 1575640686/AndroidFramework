package com.kevin.framework.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {
	/**
	 * 检查 是否 连接 2G3G
	 * 
	 * @param context
	 * @return
	 */
	public static boolean check2G3G4G(Context context) {
		return checkNet(context, ConnectivityManager.TYPE_MOBILE);
	}

	/**
	 * 检查是否 连接网络
	 * 
	 * @param context
	 * @return
	 */
	public static boolean checkNet(Context context) {
		return checkWifi(context) || check2G3G4G(context);
	}

	/**
	 * 检查 是否 连接 Wifi
	 * 
	 * @param context
	 * @return
	 */
	public static boolean checkWifi(Context context) {
		return checkNet(context, ConnectivityManager.TYPE_WIFI);
	}

	public static boolean checkNet(Context context, int type) { // 获取手机所有连接管理对象（包括对wi-fi,net等连接的管理）

		if (context == null) {
			return false;
		}
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivity == null) {
			return false;
		}
		// 获取网络连接管理的对象
		NetworkInfo info = connectivity.getNetworkInfo(type);
		if (info == null) {
			return false;
		}
		if (info.isConnected()) {
			// 判断当前网络是否已经连接
			if (info.getState() == NetworkInfo.State.CONNECTED) {
				return true;
			}
		}
		return false;
	}
}
