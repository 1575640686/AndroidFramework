package com.kevin.framework.util;

import android.util.Log;

public class LogUtil {
	private static int LEVEL = Log.INFO;
	
	public static void setDebug(boolean isDebug) {
		if(isDebug == true) {
			LEVEL = Log.DEBUG;
		} else {
			LEVEL = Log.INFO;
		}
	}

	public static void d(String tag, String msg) {
		log(Log.DEBUG, tag, msg);
	}
	
	public static void i(String tag, String msg) {
		log(Log.INFO, tag, msg);
	}

	public static void e(String tag, String msg) {
		log(Log.ERROR, tag, msg);
	}

	public static void w(String tag, String msg) {
		log(Log.WARN, tag, msg);
	}

	public static void log(int priority, String tag, String msg) {
		if (priority < LEVEL)
			return;

		String app = "[APP]";

		StringBuffer sb = new StringBuffer();
		sb.append(app);
		sb.append(msg);
		String message = sb.toString();
		if (priority == Log.DEBUG) {
			Log.d(tag, message);
		}
		if (priority == Log.INFO) {
			Log.i(tag, message);
		}
		if (priority == Log.ERROR) {
			Log.e(tag, message);
		}
		if (priority == Log.WARN) {
			Log.w(tag, message);
		}
		if (priority == Log.VERBOSE) {
			Log.v(tag, message);
		}
	}
}
