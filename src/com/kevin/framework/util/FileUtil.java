package com.kevin.framework.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.EncodingUtils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

@SuppressWarnings("deprecation")
public class FileUtil {
	
	private static final String TAG = "FileUtil"; 

	/**
	 *  获取 目录的 绝对路径
	 * @param path 目录路径
	 */
	public static String getDirectoryAbsolutePath(String path) {
		if(!checkSdStatus()) {
			return null;
		}
		File dir = new File(getSdDirectoryPath(), path);
		if(dir != null) {
			if(!dir.mkdirs()) {
				if(!dir.exists()) {
					LogUtil.e(TAG, "目录创建失败：" + dir.getPath());
					return null;
				}
			}
		}
		return dir.getAbsolutePath();
	}

	/**
	 * 获取SD 卡 路径
	 */
	public static File getSdDirectoryPath() {
		return Environment.getExternalStorageDirectory();
	}
	
	/**
	 *  获取 机身路径
	 */
	public static File getSystemDirectoryPath() {
		return Environment.getRootDirectory();
	}
	
	public static boolean checkSdStatus() {
		if(Environment.getExternalStorageState().equals(
		        android.os.Environment.MEDIA_MOUNTED)) { //需要判断手机上面SD卡是否插好
			return true;
		}
		return false;
	}
	
	/**
	 *  获取块大小 
	 */
	public static long getSdBlocSize() {
		if(!checkSdStatus()) {
			return 0;
		}
		StatFs statfs = new StatFs(getSdDirectoryPath().getPath());
		return statfs.getBlockSize();
	}
	
	/**
	 *  获取块大小 
	 */
	public static long getSystemBlocSize() {
		StatFs statfs = new StatFs(getSystemDirectoryPath().getPath());
		return statfs.getBlockSize();
	}
	
	/**
	 * 获取总块数
	 */
	public static long getSdBlockCount() {
		if(!checkSdStatus()) {
			return 0;
		}
		StatFs statfs = new StatFs(getSdDirectoryPath().getPath());
		return statfs.getBlockCount();
	}
	
	/**
	 * 获取总块数
	 */
	public static long getSystemBlockCount() {
		StatFs statfs = new StatFs(getSystemDirectoryPath().getPath());
		return statfs.getBlockCount();
	}
	
	/**
	 * 获取可用块数
	 */
	public static long getSdAvailableBlocks() {
		if(!checkSdStatus()) {
			return 0;
		}
		StatFs statfs = new StatFs(getSdDirectoryPath().getPath());
		return statfs.getAvailableBlocks();
	}
	
	/**
	 * 获取可用块数
	 */
	public static long getSystemAvailableBlocks() {
		StatFs statfs = new StatFs(getSystemDirectoryPath().getPath());
		return statfs.getAvailableBlocks();
	}
	
	/**
	 * /**
	 *  获取SD 卡 全部空间大小
	 * @return 单位： Kb
	 */
	public static long getSDTotalSize() {
		return getSdBlocSize() * getSdBlockCount() / 1024;
	}
	
	/**
	 *  获取SD 卡 剩余空间大小
	 *   @return 单位： Kb
	 */
	public static long getSDRemainingSize() {
		return getSdAvailableBlocks() * getSdBlocSize() / 1024;
	}
	
	/**
	 *  获取机身 全部空间大小
	 *  @return 单位： Kb
	 */
	public static long getSystemTotalSize() {
		return getSystemBlocSize() * getSystemBlockCount() / 1024;
	}
	
	/**
	 *  获取机身 剩余空间大小
	 *   @return 单位： Kb
	 */
	public static long getSystemRemainingSize() {
		return getSystemAvailableBlocks() * getSystemBlocSize() / 1024;
	}
	
	/**
	 *  获取目录下全部 以 .txt 结尾的文件 
	 * @param path 目录路径
	 */
	public static List<String> getAllFileByFileExtension(String path) {
		List<String> pathlist = new ArrayList<String>();
		File file = new File(path);
		if (file.isDirectory()) {
			File[] dirFile = file.listFiles();
			for (File f : dirFile) {
				if (f.isDirectory()) {
					getAllFileByFileExtension(f.getAbsolutePath());
				} else {
					if (f.getName().endsWith(".txt")) {
						pathlist.add(f.getAbsolutePath());
					}
				}
			}
		}
		return pathlist;
	}
	
	/**
	 *  获取目录 及子目录 全部文件 
	 * @param path 目录路径
	 */
	public static List<String> getAllFile(String path) {
		List<String> pathlist = new ArrayList<String>();
		File file = new File(path);
		if (file.isDirectory()) {
			File[] dirFile = file.listFiles();
			for (File f : dirFile) {
				if (f.isDirectory()) {
					getAllFileByFileExtension(f.getAbsolutePath());
				}
				pathlist.add(f.getAbsolutePath());
			}
		}
		return pathlist;
	}
	
	/**
	 * 读文件
	 * @param fileName
	 * @return
	 */
	public static String readSDFile(String fileName) {
		String res = null;
		FileInputStream fis = null;
		try {
			File file = new File(fileName);
			
			fis = new FileInputStream(file);
			
			int length = fis.available();
			
			byte[] buffer = new byte[length];
			
			fis.read(buffer);
			
			res = EncodingUtils.getString(buffer, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return res;
	}
	
	/**
	 *  写文件
	 * @param fileName
	 * @param write_str
	 */
	public static void writeFileToSD(String fileName, String write_str) {
		FileOutputStream fos = null;
		try {
			File file = new File(fileName);

			fos = new FileOutputStream(file);

			byte[] bytes = write_str.getBytes();

			fos.write(bytes);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 *  读取assets 目录里的文件
	 * @param FileName 文件名字
	 * @param context 上下文
	 * @return	文件内容
	 */
	public static String getFileContentFromAssets(Context context, String FileName) {
		if(context == null) {
			return null;
		}
		String result = "";
		try {
			InputStream in = context.getAssets().open(FileName);// 获取资源
			int length = in.available();		// 获取文字字数
			byte[] buffer = new byte[length];
			in.read(buffer);		// 读到数组中
			result = EncodingUtils.getString(buffer, "UTF-8");		// 设置编码
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 *  删除文件
	 * @param path 文件路径
	 * @throws FileNotFoundException
	 */
	public static void deletefile(String path) throws FileNotFoundException {
		File file = new File(path);
		if (file.isFile()) {
			if(file.delete()) {
				LogUtil.d(TAG, "File " + path + " 文件删除成功!!!");
			}
		}
	}
	

}
