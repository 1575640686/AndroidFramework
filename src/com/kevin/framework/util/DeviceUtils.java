package com.kevin.framework.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;

public class DeviceUtils {
	
	/**
	 * 获取应用程序的IMEI号
	 * @param context   上下文
	 */
	public static String getIMEI(Context context) {
		if (context == null) {
			return null;
		}
		TelephonyManager telecomManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		return telecomManager.getDeviceId();
	}
	
	/**
	 * 获取设备的系统版本号
	 */
	public static int getDeviceSDK() {
		return android.os.Build.VERSION.SDK_INT;
	}
	
	 /**
     * 获取系统版本
     */
	public static String getSystemVersion() {
        return android.os.Build.VERSION.RELEASE;
    }
	
	/**
	 * 获取设备的型号
	 */
	public static String getDeviceName() {
		return android.os.Build.MODEL;
	}
	
	public static String getMacAddress() {
		String macSerial = null;
		String str = "";
		try {
			Process pp = Runtime.getRuntime().exec(
					"cat /sys/class/net/wlan0/address ");
			InputStreamReader ir = new InputStreamReader(pp.getInputStream());
			LineNumberReader input = new LineNumberReader(ir);

			for (; null != str;) {
				str = input.readLine();
				if (str != null) {
					macSerial = str.trim();// 去空格
					break;
				}
			}
		} catch (IOException ex) {
			// 赋予默认值
			ex.printStackTrace();
		}
		return macSerial;
	}
	
	public static String getMacAddress(Context context) {
		if(context == null) {
			return null;
		}
		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		return wifi.getConnectionInfo().getMacAddress(); 
	}

}
