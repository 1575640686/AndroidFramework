package com.kevin.framework.util;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GsonUtils {
	public static <T> T changeGsonToBean(String gsonString, Class<T> cls) {
		T t  = null;
		try {
			Gson gson = new Gson();
			t = gson.fromJson(gsonString, cls);
		} catch (Exception e) {
			e.printStackTrace();
			return t;
		}
		return t;
	}
	
	public static <T> List<T> changeGsonToList(String gsonString, Class<T> cls) {
		List<T> list = null;
		try {
			Gson gson = new Gson();
			list = gson.fromJson(gsonString, new TypeToken<List<T>>() {}.getType());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
