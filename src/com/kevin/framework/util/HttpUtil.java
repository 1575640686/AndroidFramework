package com.kevin.framework.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.protocol.HTTP;

import com.kevin.framework.Constant;

public class HttpUtil {
	private static final String TAG = "HttpUtil";
	
	/**
	 *  Http Post 请求
	 * @param url
	 * @param params
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public InputStream sendHttpClientPostRequest(String url, Map<String, String> params)  {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();					//存放请求参数
		if(params != null && !params.isEmpty()){
			for(Map.Entry<String, String> entry : params.entrySet()) {
				pairs.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
			} 
		}
		try {
			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(pairs, HTTP.UTF_8);
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(entity);
			return sendHttpRequest(httpPost);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 *   Http GET 请求
	 * @param url
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public InputStream sendHttpClientGetRequest(String url) {
		HttpGet httpGet = new HttpGet(url);
		try {
			return sendHttpRequest(httpGet);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 *   Http 请求 回调 方法判断 是否 成功
	 * @param httpRequest
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	private InputStream sendHttpRequest(HttpUriRequest httpRequest) throws IOException,
			ClientProtocolException {
		
		HttpClient httpClient = new DefaultHttpClient();
		httpClient.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, Constant.CONNECT_TIMEOUT);	// 设置请求超时时间
		httpClient.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT, Constant.CONNECT_TIMEOUT); 			// 读取超时

		HttpResponse httpResponse = httpClient.execute(httpRequest);
		int code = httpResponse.getStatusLine().getStatusCode();
		if (code == HttpStatus.SC_OK) {
            HttpEntity httpEntity = httpResponse.getEntity();	// 这里只能获取一次，第二次就获取不到
            if (httpEntity != null) {
            	LogUtil.d(TAG, "HTTP("+httpRequest.getMethod()+")发送请求["+httpRequest.getURI()+"] 正常返回");

                return httpEntity.getContent();
            } else {
            	LogUtil.e(TAG, "HTTP("+httpRequest.getMethod()+")请求["+httpRequest.getURI()+"]不正常，Status Code 200, entity为空");
            }
        } else if(code == HttpStatus.SC_REQUEST_TIMEOUT) {
        	LogUtil.e(TAG, "HTTP("+httpRequest.getMethod()+")请求["+httpRequest.getURI()+"]不正常，Status Code 408, 网络超时");
        } else {
        	LogUtil.e(TAG, "HTTP("+httpRequest.getMethod()+")请求["+httpRequest.getURI()+"]不正常，Status Code :" + code);
        }
		return null;
	}
}
