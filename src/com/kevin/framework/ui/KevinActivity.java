package com.kevin.framework.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

/**
 * 如果使用 注解 {@link com.kevin.framework.view.animation.ContentView}
 * 继承该activity
 * @author tianshaokai
 *
 */
public abstract class KevinActivity extends Activity {
	protected Context mContext;
	
	protected void startActivity(Context context, Class<?> clazz) {
		Intent intent = new Intent(context, clazz);
		startActivity(intent);
	}
	
	protected void startActivity(String action) {
		Intent intent = new Intent(action);
		startActivity(intent);
	}
	
	protected void startActivity(String action, Uri uri) {
		Intent intent = new Intent(action, uri);
		startActivity(intent);
	}

	protected void startActivity(Context context, Class<?> clazz, Bundle bundle) {
		Intent intent = new Intent(context, clazz);
		intent.putExtras(bundle);
		startActivity(intent);
	}
	
	protected void startActivityForResult(Context context, Class<?> clazz,
			int requestCode) {
		Intent intent = new Intent(context, clazz);
		startActivityForResult(intent, requestCode);
	}
	
	protected void startActivityForResult(Context context, Class<?> clazz,
			Bundle bundle, int requestCode) {
		Intent intent = new Intent(context, clazz);
		intent.putExtras(bundle);
		startActivityForResult(intent, requestCode);
	}

}
