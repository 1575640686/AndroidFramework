package com.kevin.framework.ui;


/**
 * 普通 activity 继承该activity
 * @author tianshaokai
 */
public abstract class BaseActivity extends KevinActivity {
	
	protected abstract void findView();

}
