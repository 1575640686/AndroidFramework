package com.kevin.framework;

public class Constant {

	public static final int TOAST_IN_TOP = 0;
	public static final int TOAST_IN_CENTER = 1;
	public static final int TOAST_IN_BOTTOM = 2;
	
	/** 时间超时 */
	public static final int CONNECT_TIMEOUT = 20000;
	
	/** 判断是否连接网络 */
	public static boolean isConnectionNetwork = true;
	
	public static boolean isWifi = true;
	
	public static boolean isMobile = true;
}
