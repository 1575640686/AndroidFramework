package com.kevin.framework.http;

public interface AsyncTaskCallBack {
	public void onPostExecute(Object result);
}
