package com.kevin.framework.http;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.kevin.framework.util.HttpUtil;

/**
 * 抽象  异步任务 Post请求
 * 
 * @author tianshaokai
 * 
 * @param <Params>
 *            参数
 * @param <Progress>
 * @param <Result>
 *            结果集
 */
public abstract class AbstractPostAsyncTask<Params, Progress, Result> extends
		AsyncTask<Params, Progress, Result> {
	
	private HttpUtil mHttpUtil;
	private AsyncTaskCallBack mAsyncTaskCallBack;
	private Class<Result> result;
	private Gson gson;

	public AbstractPostAsyncTask(Class<Result> result,
			AsyncTaskCallBack mAsyncTaskCallBack) {
		this.mAsyncTaskCallBack = mAsyncTaskCallBack;
		this.result = result;
		this.gson = new Gson();
		this.mHttpUtil = new HttpUtil();
	}
	
	@Override
	protected Result doInBackground(Params... params) {
		InputStream is = mHttpUtil.sendHttpClientPostRequest(getRequestUrl(), getRequestParams(params));
		return parse(is, result);
	}
	
	private Result parse(InputStream is, Class<Result> t) {
		Result t1 = null;
		try {
			Reader reader = new InputStreamReader(new BufferedInputStream(is));
			t1 = gson.fromJson(reader, t);
			return t1;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (is != null) {
					is.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return t1;
	}
	
	@Override
	protected void onPostExecute(Result result) {
		super.onPostExecute(result);
		mAsyncTaskCallBack.onPostExecute(result);
	}
	
	public abstract Map<String, String> getRequestParams(Params... params);
	
	public abstract String getRequestUrl();

}
